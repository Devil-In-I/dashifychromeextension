const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: {
    popup: './src/popup/popup.jsx',
    background: './src/background/background.jsx',
    content: './src/content/content.jsx',
    InjectContent: './src/content/InjectContent.jsx'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [{
        test: /\.(js|jsx)$/,
        use: {
            loader: 'babel-loader',
            options:{
                presets: ['@babel/preset-env', '@babel/preset-react'],
            }
        },
        exclude: /node_modules/,
    }],
  },
  plugins: [
    new HtmlWebpackPlugin({
    template: './src/popup/popup.html',
    filename: 'popup.html',
  }),
    new CopyPlugin({
    patterns: [
      { from: "public" },
    ],
  }),
],
};
