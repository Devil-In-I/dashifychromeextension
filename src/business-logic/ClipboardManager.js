export async function TakeTextFromClipboardAsync() {
    try {
        var text = await navigator.clipboard.readText();
        console.log(`Text: ${text}`);
        return text;    
    } catch (error) {
        console.error();
    }
    return "";
}

export async function InsertTextToClipboardAsync(text) {
    if (text === null || text.length <= 0 || text === " ") {
        throw new Error("Text must not be null or empty.");
    }
    await navigator.clipboard.writeText(text);
}