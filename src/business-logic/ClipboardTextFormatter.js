import { TakeTextFromClipboardAsync, InsertTextToClipboardAsync} from './ClipboardManager';
import { DashifyText, AddUsNumToTemplate } from './TextFormatter';

export function DashifyTextFromClipboard() {
    TakeTextFromClipboardAsync()
    .then((text) => {
        try {
            console.log(text);
            let dashifiedText = DashifyText(text);
            InsertTextToClipboardAsync(dashifiedText);
        } catch (error) {
            console.error(`Unexpected error occurred while trying to dashify text. Details ${error}`);
        }
    })
    .catch( error => console.error(error));
}

export function ConvertToInvestigationTaskTemplate() {
    TakeTextFromClipboardAsync()
    .then((text) => {
        try {
            if (/^\d+$/.test(text.trim())){
                let transformedText = AddUsNumToTemplate(text);
                InsertTextToClipboardAsync(transformedText);
            } else {
                console.error("The value in the clipboard is not a valid number");
            }
        } catch (error) {
            console.error(`Unexpected error occurred while trying to transform text. Details ${error}`);
        }
    })
    .catch(error => console.error(error));
}