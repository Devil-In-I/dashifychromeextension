export function DashifyText(text) {
    if (text === null || text.length <= 0 || text === " ") {
        throw new Error("Text must not be null or empty.");
    }
    return text.trim().replaceAll(' ', '-');
}

export function AddUsNumToTemplate(USNumber) {
    if (USNumber === null || USNumber.length <= 0) {
        throw new Error("USNumber must not be null or empty.");
    }
    return `Investigate US ${USNumber.trim()}`;
}
  