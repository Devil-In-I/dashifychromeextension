function handleModalWindowAppeared() {
  AddPrefixToInput('feature/');
  addCustomButtonToModal();
  HandleWritingTextToInput();
  // BUG: add a try-catch block or condition to avoid errors in the console when some other modals appear.
}

function HandleWritingTextToInput(){
  const inputElement = document.querySelector(".item-name-input");

  inputElement.addEventListener('input', function(event) {
    const inputValue = event.target.value;
    const modifiedValue = inputValue.replace(/\s+/g, '-');
    
    var button = document.getElementById('__bolt-create-dialog-dashify-text');

    if (button.className != 'bolt-button enabled primary bolt-focus-treatment') {

      button.className = 'bolt-button enabled primary bolt-focus-treatment';
      console.log("Button is active now");
    }

    event.target.value = modifiedValue;
  });

  inputElement.addEventListener('paste', function(event) {
    event.preventDefault(); // Prevent the default paste behavior

    const clipboardData = event.clipboardData || window.clipboardData;
    const pastedText = clipboardData.getData('text/plain');
    const modifiedText = pastedText.trim().replace(/\s+/g, '-');

    document.execCommand('insertText', false, modifiedText);
  });
}

function handleButtonClick () {
  var input = document.querySelector(".item-name-input");
  var text = input.value;
  if (text != null && text.length > 0 && text != " ") {
    input.value = text.trim().replaceAll(' ', '-');
    var createButton = document.getElementById("__bolt-create-dialog-confirm");
    createButton.classList.remove("disabled");
    console.log("Create button activated.");
    var errorMessage = document.getElementById("__bolt-form-item-1-message");
    if (errorMessage !== null) {
      errorMessage.remove();
    }
    console.log("Error message removed.");  
  }
}

function AddPrefixToInput(prefix){
  var input = document.getElementsByClassName("item-name-input")[0];
  if (input != null) {
    input.value = prefix;
  }
}

function createCustomButton(){
  const customButton = document.createElement('button');
    customButton.className = 'bolt-button disabled bolt-focus-treatment';
    customButton.setAttribute('data-focuszone', 'focuszone-46');
    customButton.setAttribute('data-is-focusable', 'true');
    customButton.id = '__bolt-create-dialog-dashify-text';
    customButton.setAttribute('role', 'button');
    customButton.setAttribute('tabindex', '0');
    customButton.type = 'button';
    customButton.title = "I will add a functionality, when remember what exactly i wanted to do with it. :)";
    customButton.onclick = () => {
      handleButtonClick();
    }
    return customButton;
}

function addCustomButtonToModal() {
  const modal = document.querySelector('.bolt-portal.absolute-fill');
  if (modal && !document.getElementById('__bolt-create-dialog-dashify-text')) {
    var createBrunchTemplateButton = createCustomButton();

    const spanElement = document.createElement('span');
    spanElement.className = 'bolt-button-text body-m';
    spanElement.textContent = 'Dashify branch name';

    createBrunchTemplateButton.appendChild(spanElement);

    //Adding a button to a Div from the DOM
    const buttonsDiv = document.querySelector('.bolt-panel-footer-buttons.flex-grow.bolt-button-group.flex-row');
    buttonsDiv.appendChild(createBrunchTemplateButton);
  }
}

// BUG: Some other bug i don't remember, but it exists..
// BUG: When clicking custom button to dashify branch name - text from input disappears!
// TODO: Add the same logic to get a modal but on different page
// TODO: To think on better solution for this in a generic method, or something like that.

// Object to track DOM changes
const observer = new MutationObserver((mutationsList) => {
  for (const mutation of mutationsList) {
    if (mutation.addedNodes.length && mutation.type === 'childList') {
      for (const node of mutation.addedNodes) {
        if (node.classList &&
            node.classList.contains('bolt-portal') &&
            node.classList.contains('absolute-fill')) {
          handleModalWindowAppeared();
        }
      }
    }
  }
});


const elem = document.querySelector(".bolt-portal-host.absolute-fill.no-events.scroll-hidden")
// If such element exists in DOM, than start tracking its changes.
if  (elem) {
  observer.observe(elem, { childList: true, subtree: true });
}
