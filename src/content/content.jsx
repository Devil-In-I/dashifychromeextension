import { DashifyTextFromClipboard, ConvertToInvestigationTaskTemplate } from '../business-logic/ClipboardTextFormatter';

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.message === "DashifyTextFromClipboard") {
            DashifyTextFromClipboard();
        }
        else if (request.message === "ConvertToInvestigationTaskTemplate") {
            ConvertToInvestigationTaskTemplate();
        }
    }
);