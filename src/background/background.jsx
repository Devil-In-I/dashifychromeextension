chrome.commands.onCommand.addListener((command) => {
  switch (command) {
    case "dashify_text_from_clipboard":
      DashifyTextFromClipboard();
      break;

    case "insert_copied_text_to_template":
      ConvertToInvestigationTaskTemplate();
      break;
    default:
      break;
  }
});

function ConvertToInvestigationTaskTemplate() {
  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
    if (tabs && tabs.length > 0) {
      chrome.tabs.sendMessage(tabs[0].id,
        {
          message: 'ConvertToInvestigationTaskTemplate'
        }, function(response){})
    }
    else{
      console.log("There's no active tab.");
    }
  });
}

function DashifyTextFromClipboard() {
  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
    if (tabs && tabs.length > 0) {
      chrome.tabs.sendMessage(tabs[0].id,
        {
          message: 'DashifyTextFromClipboard'
        }, function(response){})
    }
    else{
      console.log("There's no active tab.");
    }
  });
}