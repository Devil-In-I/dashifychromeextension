import React from 'react';
import ReactDOM from 'react-dom/client';

function Popup() {

  return (
    <div style={{width: "300px", height:"auto"}}>
      <h3>Something definitely should be here, but i just don't know what exactly yet.<br/> 
        <small className='text-muted'>Peace! ♪♥☺</small></h3>
    </div>
  );
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<Popup />);
